
// GLOBALS

var data = data || {};

data.lists = {
  ships: [],
  ports: [],
  surnames: [],
  years: []
};

data.display = {
  ship: {},
  surname: {},
  convict: {},
  year: {}
};

data.logs = [];

var debug = true;
var db = null;

// Add string formatting
String.format = function() {
  var s = arguments[0];
  for (var i = 0; i < arguments.length - 1; i++) {       
    var reg = new RegExp("\\{" + i + "\\}", "gm");             
    s = s.replace(reg, arguments[i + 1]);
  }

  return s;
}

var _console_log = console.log;
if (debug) {
  console.log = function(log) {
    data.logs.push({data: log, timestamp: (new Date()).getTime()});
     _console_log.apply(console, arguments);
  };
} else {
  $('#menu-logs').css('display','none');
}


// Add cordova reference dynamically
if (navigator.userAgent.match(/android/i)) {
  console.log('navigator.userAgent.match(/android/i)');

  // Wait for Cordova to load 
  document.addEventListener("deviceready", function() {
    console.log('deviceready()');

    // Add SQLLite support
    if (window.sqlitePlugin) {
      db = window.sqlitePlugin.openDatabase({name: "convicts.1.1.db", createFromLocation: 1});
      console.log('window.sqlitePlugin.openDatabase()');
    } else {
      console.log('window.sqlitePlugin.openDatabase == undefined;');
    }

    if (device && device.version) {
      if (Number((device.version + "00000").replace(/[^\d]/g, '').substring(0,5)) < 42000) {
        $('#version-alert').css('display','block');
      }
    }

  }, false);

} else {

  console.log('!navigator.userAgent.match(/android/i)');
  db = window.openDatabase('convicts.db', '1.0', 'Convicts DB', 40000000);
  console.log('window.openDatabase()');

}


(function() {


  // Add jQuery Rendering via Handlebars.js
  /*
  if (Handlebars) {
      $.fn.extend({
          render: function(data) {
              console.log('$.fn.render(data)', this, this.id, data);
              var html, dom = $(this)[0], templates = $.fn.render.templates;
              if (!dom || !dom.id) return this;
              if (!templates[dom.id]) { // First time compile template
                  templates[dom.id] = Handlebars.compile($(this).html());
              } 
              var html = templates[dom.id](data || '');
              $(this).html(html);
              return this;
          }
      });
      $.fn.render.templates = {};
      var helpers = {
          addCommas: function (number) {
              return number.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
          }
      };
      for(var helper in helpers) {
          Handlebars.registerHelper(helper, helpers[helper]);
      }
  }

  console.log('Handlebars initialised');
  */


  /*
  // Prep database data
  db.transaction(function (tx) {
      data = {}; 
     tx.executeSql('SELECT * FROM ships', [], function (tx, results) {
        data.ships = results.rows;
        console.log('rendering ship list');
     }, null);
     tx.executeSql('SELECT * FROM ports', [], function (tx, results) {
        data.ports = results.rows;
     }, null);
     tx.executeSql('SELECT * FROM surnames', [], function (tx, results) {
        data.surnames = results.rows;
     }, null);
     tx.executeSql('SELECT * FROM years', [], function (tx, results) {
        data.years = results.rows;
     }, null);
  });

  console.log('Data initialised');

  */



  // Preprocess the main lists
  for(var item in data.ships) data.lists.ships.push({name: item, convicts: data.ships[item]});
  for(var item in data.ports) data.lists.ports.push({name: item, convicts: data.ports[item]});
  for(var item in data.surnames) data.lists.surnames.push({name: item, convicts: data.surnames[item]});
  for(var item in data.years) {
    var schedule = data.years[item], arrivals = [], convicts = 0;
    for (var date in schedule) {
      arrivals.push({name: date, convicts: schedule[date]});
      convicts += schedule[date];
    }
    data.lists.years.push({name: item, convicts: convicts, arrivals: arrivals});
  }

  //$('ul#surnames-list').render(data.lists.surnames);
  //console.log($('ul#surnames-list').html());
  //console.log(data.lists.surnames.length);

  /* // Raw DOM

  $('ul#ships-list').render(data.lists.ships);
  console.log($('ul#ships-list').html());

  */


  var hash = '9axcrs8t32rb7l4c9p9';
  var module = ons.bootstrap('myApp', ['onsen']);

  $('#surnames-page').click(function() {alert('click');});


  // Scroll past the end of the page
  // used to minimise uneccessary DOM
  module.directive('flexScroll', function() {
      return {
          link: function(scope, element, attr) {
              console.log('AppController.flexScroll()');
               var pageHeight = $('div.page__content', element[0]).height() + 100;
               $('div.page__content', element[0]).on('scroll', function(e) {
                  // Progressively increase list as
                  // user scrolls to bottom
                  var minHeight = attr.flexScroll;
                  var bottomHeight = e.currentTarget.scrollHeight - pageHeight;
                  if (!scope.isFiltered && bottomHeight > minHeight && $(e.currentTarget).scrollTop() > bottomHeight) {
                      console.log('scrolled to bottom!', scope.numLimit);
                      scope.$apply(function() {
                        scope.numLimit += scope.increase;
                      });
                  }
              });
          }
      };
  });


  // Click to close (popup)
  module.directive('clickClose', function() {
    return {
      link: function(scope, element, attr) {
        console.log('AppController.clickClose()');
        $(element).click(function(e) {
          $(e.currentTarget).hide();
        });
      }
    };
  });


  // Filters only after 3 letters
  module.filter('filterMin3',[ function () {
    return function(items, searchText) {
        var filtered = [];
        if (!searchText || searchText.length < 3) return items;
        searchText = searchText.toLowerCase();
        items.forEach(function(item, index, array) {
            var name = item.name.toLowerCase();
            if (name.indexOf(searchText) == 0)
                filtered.push(item);
        });
        return filtered;
    };
  }]);


  // Main App Controller
  module.controller('AppController', function($scope) { 

    console.log('AppController.init()');

    $scope.ports = data.lists.ports;
    $scope.ships = data.lists.ships;
    $scope.years = data.lists.years;
    $scope.numLimit = 200;
    $scope.increase = $scope.numLimit;

    $scope.show = function(type, name) {
      console.log('AppController.show()', type, name)

      var image;
      switch(type) {
        case 'ship':
          image = 'ship.png'; break;
        case 'port':
          image = 'ports.png'; break;
        case 'year':
          image = 'calendar.png'; break; 
        case 'surname':
          image = 'chaingang.png'; 
          type = 'lastname';
          break;
        default:
          image = 'ships.png'; break;
      }
      db.transaction(function (tx) {
         tx.executeSql('SELECT * FROM convicts WHERE `' + type + '` = ? ORDER BY lastname LIMIT 1200;', [name], function (tx, results) {
            console.log('DB.executeSql()', results.rows.length);
            if (results) {
              var convicts = [];
              for(var i = 0; i < results.rows.length; i++) {
                convicts.push(results.rows.item(i));
              }
              data.display.filter = {
                  name: name,
                  number: i,
                  image: image,
                  convicts: convicts
              };
              historyStack.pushPage('filter.html')
            }
         }, null);
      });

    };

  });


  // Filter for Log section (ie 2 minutes ago)
  module.filter('timeago', function() {
    return function (item, input) {
      return moment(item).fromNow()
    };
  });


/*
  module.filter('search', function() {
    return function (items, input) {
      var output = [], scope = input.scope, type = input.type;
      var pattern = new RegExp(scope.surnameQuery, 'i');
      scope.isFiltered = !!scope.surnameQuery;
      console.log('searchSurnames', scope.isFiltered, scope.surnameQuery);
      return items;
      if (scope.isFiltered) {
          // Apply filtering to all surnames 
          // not just the ones being displayed
          for (var i = 0; i < scope.all_surnames.length; i++) {
            var item = scope.all_surnames[i];
            if (!pattern || pattern.test(item.name)) {
              output.push(item);
            }
          }
          return output.slice(0, 500);
      } else {
          // If not filtered just return whatever is cached
          // this will change as you scroll down the page
          return items;
      }
    };
  });
*/

  // Controller for the list of surnames
  module.controller('SurnamesController', function($scope) {
      
      console.log('SurnamesController.init()');
      $scope.surnames = data.lists.surnames;

  });


  // Controller for filter page
  module.controller('FilterController', function($scope) {
    console.log('FilterController.init()');

    $scope.filter = data.display.filter;

    $scope.show = function(convict) {
      console.log('FilterController.show()', convict);
      data.display.convict = convict;
      db.transaction(function (tx) {
         tx.executeSql('SELECT * FROM refs WHERE `id` = ? ORDER BY type;', [convict.id], function (tx, results) {
            console.log('DB.executeSql()', results.rows.length);
            if (results) {
              var refs = [];
              for(var i = 0; i < results.rows.length; i++) {
                refs.push(results.rows.item(i));
                refs[i].image = 'book' + Math.ceil(Math.random() * 5);
              }
              data.display.convict.refs = refs;
              historyStack.pushPage('convict.html');
            }
         }, null);
      });
    };
  });


  // Controller for main convict page
  module.controller('ConvictController', function($scope, $sce) {
    console.log('ConvictController.init()');
    $scope.convict = data.display.convict;
    $scope.showref = function(ref) {
      if (!ref) return;
      console.log('ConvictController.showref()', ref.id);
      $scope.showpopover('Opening digitised record ...');
      window.setTimeout(function() {
        if (ref.hasimage) {
          window.open(String.format('http://search.archives.tas.gov.au/ImageViewer/images/{0}/large/{0}_{1}_L.JPG', ref.code, ('0000' + ref.page).slice(-5)), '_blank', 'location=no');
        } else {
          window.open(String.format('http://search.archives.tas.gov.au/default.aspx?detail=1&type=I&id={0}', ref.code), '_blank', 'location=no');
        }
        $scope.hidepopover();
      }, 1000)
    }
    $scope.searchtrove = function(convict) {
      console.log('ConvictController.searchtrove()', convict.firstname, convict.lastname);
      $scope.showpopover('Searching National Library of Australia for newspaper articles ...');
      $.ajax({
        url: String.format('http://api.trove.nla.gov.au/result?key={0}&zone=newspaper&q=%22{1}+{2}%22+Tas+date:[{3}%20TO%20{4}]', hash.substr(7 % 4), convict.firstname, convict.lastname, convict.year, convict.year + 15),
        success: function(result) {
          console.log('ConvictController.searchtrove().success()');
          var article, articles = [], xml_articles = $('article', result);
          console.log('found ' + xml_articles.length + ' articles');
          $(xml_articles).each(function(i, e) {
            article = $.xml2json(e).article;
            article.snippet = $sce.trustAsHtml(article.snippet);
            articles.push(article);
          });
          $('#trove-button').hide();
          $scope.$apply(function() {
            $scope.articles = articles;
          });
        },
        error: function() {
          console.log('ConvictController.searchtrove().error()');
        },
        timeout: function() {
          console.log('ConvictController.searchtrove().timeout()');
        },
        complete: function() {
          $scope.hidepopover();
        }
      });
    };
    $scope.showpopover = function(message) {
      console.log('ConvictController.showpopover()');
      $('#popover-message-text').text(message);
      $('#popover-message').show();
    }
    $scope.hidepopover = function() {
      console.log('ConvictController.hidepopover()');
      $('#popover-message').hide();
    }
    $scope.openarticle = function(article) {
      console.log('ConvictController.openarticle()', article.troveUrl);
      window.open(article.troveUrl, '_blank', 'location=no');
    }
  });


  module.controller('LogsController', function($scope) {
    console.log('LogsController.init()');
    $scope.logs = data.logs;
  });

  // Initialise UI

  ons.ready(function() {

    console.log('ons.ready()');

    window.setTimeout(function() {
        $('#splash').fadeOut();
        $('#app').fadeIn();
    }, 500);

  });


})();