<!DOCTYPE html>
<!-- CSP support mode (required for Windows Universal apps): https://docs.angularjs.org/api/ng/directive/ngCsp -->
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="mobile-web-app-capable" content="yes" />

    <!-- JS dependencies (order matters!) -->
    <script src="scripts/platformOverrides.js"></script>
    <script src="lib/angular/angular.js"></script>
    <script src="lib/jquery/jquery.1.9.1.min.js"></script>
    <script src="lib/handlebars-v3.0.1.min.js"></script>
    <script src="lib/async.1.3.0.min.js"></script>
    <script src="lib/onsen/js/onsenui.js"></script>

    <!-- CSS dependencies -->
    <link rel="stylesheet" href="lib/onsen/css/onsenui.css" />
    <link rel="stylesheet" href="lib/onsen/css/onsen-css-components.css" />

    <!-- --------------- App init --------------- -->

    <title>Tasmanian Convicts Pocket Reference</title>

    <link rel="stylesheet" href="style.css" />
    <style>
    ::-webkit-scrollbar {
        display: block !important;
        width: 5px;
        height: 0px;
    }

    ::-webkit-scrollbar-track {
        background: rgba(0,0,0,0.1);
    }

    ::-webkit-scrollbar-thumb {
        border-radius: 2px;
        background: rgba(0,0,0,0.3);
        -webkit-box-shadow: inset 0 0 3px rgba(0,0,0,0.5); 
    }

    </style>

</head>

<body ng-controller="AppController">
    <script src="scripts/index.js"></script>
    <!-- -->
    <div id="splash">
        <p style="text-align:center;"><img src="img/convicts.png" style="width:300px;"/></p>
        <h2>Tasmanian Convicts Pocket Reference</h2>
        <p style="color: #999; font-size: 13px;">A pocket guide of the arrival and history of <br/>85,000 Tasmanian convicts (1803-1893).</p>
    </div>

    <div id="app" style="display: none">

    <ons-sliding-menu menu-page="menu.html" main-page="main.html" side="right"
                      var="menu" type="reveal" max-slide-distance="260px" swipeable="true">
    </ons-sliding-menu>

    <ons-template id="menu.html">
        <ons-page modifier="menu-page">
            <ons-list class="menu-list">
                <ons-list-item class="menu-item" ng-click="menu.setMainPage('main.html', {closeMenu: true});">
                    <ons-icon icon="fa-home" size="16px"></ons-icon> &nbsp; Main
                </ons-list-item>

                <ons-list-item class="menu-item" ng-click="menu.closeMenu(); historyStack.pushPage('history.html');">
                    <ons-icon icon="fa-clock-o" size="16px"></ons-icon> &nbsp; History
                </ons-list-item>

                <ons-list-item class="menu-item" ng-click="menu.closeMenu(); historyStack.pushPage('ships.html');">
                    <ons-icon icon="ion-android-boat" size="16px"></ons-icon> &nbsp; Ships
                </ons-list-item>

                <ons-list-item class="menu-item" ng-click="menu.closeMenu(); historyStack.pushPage('ports.html');">
                    <ons-icon icon="fa-globe" size="16px"></ons-icon> &nbsp; Departure Ports
                </ons-list-item>

                <ons-list-item class="menu-item" ng-click="menu.closeMenu(); historyStack.pushPage('surnames.html');">
                    <ons-icon icon="fa-user" size="16px"></ons-icon> &nbsp; Surnames
                </ons-list-item>

                <ons-list-item class="menu-item" ng-click="menu.closeMenu(); historyStack.pushPage('calendar.html');">
                    <ons-icon icon="fa-calendar" size="16px"></ons-icon> &nbsp; Calendar
                </ons-list-item>

                <ons-list-item class="menu-item" ng-click="menu.closeMenu(); historyStack.pushPage('credits.html');">
                    <ons-icon icon="fa-hand-o-right" size="16px"></ons-icon> &nbsp; Credits
                </ons-list-item>

                <ons-list-item class="menu-item" ng-click="menu.closeMenu(); historyStack.pushPage('logs.html');">
                    <ons-icon icon="fa-gear" size="16px"></ons-icon> &nbsp; Logs
                </ons-list-item>

            </ons-list>

        </ons-page>
    </ons-template>

    <ons-template id="main.html">
        <ons-navigator animation="fade" var="historyStack">
        <ons-page>
            <ons-row style="margin-top: 30px; text-align: center;">
                <ons-col>
                    <p style="text-align:center;"><img src="img/convicts.png" style="width:300px; "/></p>
                    <h2>Tasmanian Convicts Pocket Reference</h2>
                    <p style="color: #999; font-size: 13px;">A pocket guide of the arrival and history of <br/>85,000 Tasmanian convicts (1803-1893).</p>
                    <p><ons-button modifier="outline" ng-click="historyStack.pushPage('history.html')" style="width: 80%;">
                        <ons-icon icon="fa-clock-o" size="16px"></ons-icon> History
                    </ons-button></p>
                    <p><ons-button modifier="outline" ng-click="historyStack.pushPage('ships.html')" style="width: 80%;">
                        <ons-icon icon="ion-android-boat" size="16px"></ons-icon> Ships
                    </ons-button></p>
                    <p><ons-button modifier="outline" ng-click="historyStack.pushPage('ports.html')" style="width: 80%;">
                        <ons-icon icon="fa-globe" size="16px"></ons-icon> Departure Ports
                    </ons-button></p>
                    <p><ons-button modifier="outline" ng-click="historyStack.pushPage('surnames.html')" style="width: 80%;">
                        <ons-icon icon="fa-user" size="16px"></ons-icon> Surnames
                    </ons-button></p>
                    <p><ons-button modifier="outline" ng-click="historyStack.pushPage('calendar.html')" style="width: 80%;">
                        <ons-icon icon="fa-calendar" size="16px"></ons-icon> Calendar
                    </ons-button></p>
                </ons-col>
            </ons-row>
            <br/><br/>
        </ons-page>
        </ons-navigator>
    </ons-template>



    <ons-template id="history.html">
        <ons-page>
            <ons-toolbar>
                <div class="left"><ons-back-button>Back</ons-back-button></div>
                <div class="center">History</div>
                <div class="right">
                    <ons-toolbar-button ng-click="menu.toggle()">
                        <ons-icon icon="ion-navicon" size="28px" fixed-width="false"></ons-icon>
                    </ons-toolbar-button>
                </div>
            </ons-toolbar>

          <div class="history">

                <p class="banner"><img src="img/tasmania.png" style="width:300px;"/></p>

                <p>In <a ng-click="show('year', '1803');">1803</a>, a British expedition led by Lt. John Bowen was sent from Sydney to Tasmania (then known as Van Diemen's Land) to establish a new penal colony there.</p>

                <p class="banner"><img src="img/ship.png" style="width:300px;"/></p>

                <p>Conditions for convicts and settlers were harsh in the newly founded Van Diemen's Land. The immediate and acute problem was survival, due to lack of food and basic necessities, besides the island itself seemed a terrifying immensity of meaningless space and loneliness 'at the furthest extremity of the world'. In these circumstances the longer-term need to clear, till and plant an allocation of land was by-passed in favour of securing the one plentiful food source – kangaroos.. </p>

                <p class="banner"><img src="img/chaingang.png" style="width:300px;"/></p>

                <p>While the majority of convicts arrived from England, some came from colonies as far and wide as <a ng-click="show('port', 'Calcutta');">Calcutta</a>, <a ng-click="show('port', 'Bermuda');">Bermuda</a> and <a ng-click="show('port', 'Quebec');">Quebec</a>. After settlement, many would eventually be assigned to work for private individuals or seek permission to marry, becoming an integral part of the nascent colony. </p>

                <p class="banner"><img src="img/ports.png" style="width:300px;"/></p>

                <p>The starvation problem was soon solved as, especially in the Midlands, cleared land proved highly productive. Under Lt-Governor Arthur the economy soared, exports rose tenfold, and by the 1830s wheat and wool production along with land values were booming.</p>

                <p class="banner"><img src="img/laborers.png" style="width:300px;"/></p>

                <p>In <a ng-click="show('year', '1830');">1830</a>, the Port Arthur penal settlement was established. Due to its increased capcity, the next 20 years saw a large increase in number of transported convicts. The last convict ship from England, the <a ng-click="show('ship', 'St Vincent');">St Vincent</a>, arrived in Hobart in <a ng-click="show('year', '1853');">1853</a>, marking the official end of convict transportation, though for the next 20 years some small numbers would continue to arrive.</p>

                <p class="banner"><img src="img/calendar.png" style="width:300px;"/></p>

                <p>In total, around 85,000 convict arrivals have been recoded in the period up to <a ng-click="show('year', '1893');">1893</a>.</p>

            </div>

        </ons-page>
    </ons-template>


    <ons-template id="ships.html">
        <ons-page>
            <ons-toolbar>
                <div class="left"><ons-back-button>Back</ons-back-button></div>
                <div class="center">Ships</div>
                <div class="right">
                    <ons-toolbar-button ng-click="menu.toggle()">
                        <ons-icon icon="ion-navicon" size="28px" fixed-width="false"></ons-icon>
                    </ons-toolbar-button>
                </div>
            </ons-toolbar>

            <p class="banner"><img src="img/ships.png" style="width:300px;"/></p>

            <!--ons-list>
                <ons-list-item modifier="tappable" ons-lazy-repeat="getShip" ng-click="show(item)">
                <ons-icon icon="ion-android-boat" size="16px"></ons-icon> &nbsp; {{item}}
              </ons-list-item>
            </ons-list-->

            <!--ul id="ships-list" class="list">
                {{#if .}}
                {{#each .}}<li class="list__item list__item--tappable">{{ship}}</li>{{/each}}
                {{/if}}
            </ul-->

            <ons-list>
              <ons-list-header>Ship<span class="right">Arrivals</span></ons-list-header>
              <ons-list-item modifier="tappable" ng-click="show('ship', ship.name)" ng-repeat="ship in ::ships">
                {{ship.name}} <span class="notification">{{ship.convicts}}</span>
              </ons-list-item>
            </ons-list>

        </ons-page>
    </ons-template>



    <ons-template id="surnames.html">
        <ons-page flex-scroll="surnames">
            <ons-toolbar>
                <div class="left"><ons-back-button>Back</ons-back-button></div>
                <div class="center">Surnames</div>
                <div class="right">
                    <ons-toolbar-button ng-click="menu.toggle()">
                        <ons-icon icon="ion-navicon" size="28px" fixed-width="false"></ons-icon>
                    </ons-toolbar-button>
                </div>
            </ons-toolbar>

            <p id="chaingang" class="banner transition-out"><img src="img/chaingang.png" style="width:300px;"/></p>
            
            <div class="navigation-bar" style="padding:0 6px; border-bottom: 0;">
              <div class="navigation-bar__center">

                <input type="search" class="search-input" style="margin: 6px auto 6px auto;" placeholder="Search.." ng-model="surnameQuery" img-dissappear="chaingang">

              </div>
            </div>

            <ons-list>
              <ons-list-header>Surname<span class="right">Arrivals</span></ons-list-header>
              <ons-list-item modifier="tappable" ng-click="show('surname', surname.name)" ng-repeat="surname in surnames | search:{scope: this, type: 'surname'}">
                {{surname.name}} <span class="notification">{{surname.convicts}}</span>
              </ons-list-item>
            </ons-list>

            <!--ul id="surnames-list" class="list">
                {{#if .}}
                {{#each .}}<li class="list__item">{{name}}</li>{{/each}}
                {{/if}}
            </ul-->

        </ons-page>
    </ons-template>



    <ons-template id="calendar.html">
        <ons-page>
            <ons-toolbar>
                <div class="left"><ons-back-button>Back</ons-back-button></div>
                <div class="center">Calendar</div>
                <div class="right">
                    <ons-toolbar-button ng-click="menu.toggle()">
                        <ons-icon icon="ion-navicon" size="28px" fixed-width="false"></ons-icon>
                    </ons-toolbar-button>
                </div>
            </ons-toolbar>

            <p class="banner"><img src="img/calendar.png" style="width:300px;"/></p>

            <ons-list>
              <ons-list-header>Year<span class="right">Arrivals</span></ons-list-header>
              <ons-list-item modifier="tappable" ng-click="show('year', year.name)" ng-repeat="year in ::years">
                {{year.name}} <span class="notification">{{year.convicts}}</span>
              </ons-list-item>
            </ons-list>

        </ons-page>
    </ons-template>



    <ons-template id="ports.html">
        <ons-page>
            <ons-toolbar>
                <div class="left"><ons-back-button>Back</ons-back-button></div>
                <div class="center">Departure Ports</div>
                <div class="right">
                    <ons-toolbar-button ng-click="menu.toggle()">
                        <ons-icon icon="ion-navicon" size="28px" fixed-width="false"></ons-icon>
                    </ons-toolbar-button>
                </div>
            </ons-toolbar>

            <p class="banner"><img src="img/ports.png" style="width:300px;"/></p>

            <ons-list>
              <ons-list-header>Departure Port<span class="right">Arrivals</span></ons-list-header>
              <ons-list-item modifier="tappable" ng-click="show('port', port.name)" ng-repeat="port in ::ports">
                {{port.name}} <span class="notification">{{port.convicts}}</span>
              </ons-list-item>
            </ons-list>

        </ons-page>
    </ons-template>


    <ons-template id="credits.html">
        <ons-page id="page-credits">
            <ons-toolbar>
                <div class="left"><ons-back-button>Back</ons-back-button></div>
                <div class="center">Credits</div>
                <div class="right">
                    <ons-toolbar-button ng-click="menu.toggle()">
                        <ons-icon icon="ion-navicon" size="28px" fixed-width="false"></ons-icon>
                    </ons-toolbar-button>
                </div>
            </ons-toolbar>

          <div class="profile-card">

                <img src="img/profile.jpg" class="profile-image">

                <p>Steven de Salas (c) 2015<br/>All Rights Reserved</p>

                <p>Dataset:<br/><a href="http://data.gov.au/dataset/tasmanian-convicts-1803-1893" target="_blank">LINC Tasmania Tasmanian Convicts (1803-1893)</a></p>

                <p>APIs:<br/><a href="http://search.archives.tas.gov.au/default.aspx?detail=1&type=A&id=TA000603" target="_blank">LINC Tasmania Convict Records Online</a><br/><a href="http://trove.nla.gov.au/" target="_blank">Trove, National Library of Australia</a></p>

                <p>Images sourced from <a href="https://commons.wikimedia.org/wiki/Main_Page" target="_blank">Wikimedia</a> and available in the Public Domain</p>

                <p>Descriptions of Tasmanian Convict history from <a href="https://en.wikipedia.org/wiki/Convicts_in_Australia#Tasmania" target="_blank">Wikipedia</a> and <a href="http://www.utas.edu.au/library/companion_to_tasmanian_history/V/VDL.htm" target="_blank">University of Tasmania</a>.</p>

            </div>

        </ons-page>
    </ons-template>

    <ons-template id="logs.html">
        <ons-page ng-controller="LogsController">
            <ons-toolbar>
                <div class="left"><ons-back-button>Back</ons-back-button></div>
                <div class="center">Logs</div>
                <div class="right">
                    <ons-toolbar-button ng-click="menu.toggle()">
                        <ons-icon icon="ion-navicon" size="28px" fixed-width="false"></ons-icon>
                    </ons-toolbar-button>
                </div>
            </ons-toolbar>

            <ons-list>
              <ons-list-item ng-repeat="log in logs">
                {{log.timestamp}}: {{log.data}}
              </ons-list-item>
            </ons-list>

        </ons-page>
    </ons-template>


    <ons-template id="filter.html">
        <ons-page ng-controller="FilterController">
            <ons-toolbar>
                <div class="left"><ons-back-button>Back</ons-back-button></div>
                <div class="right">
                    <ons-toolbar-button ng-click="menu.toggle()">
                        <ons-icon icon="ion-navicon" size="28px" fixed-width="false"></ons-icon>
                    </ons-toolbar-button>
                </div>
                <div class="center">{{filter.name}} ({{filter.number}} arrivals)</div>
            </ons-toolbar>

            <p style="text-align:center;"><img src="img/{{filter.image}}" style="width:300px; "/></p>

            <ons-list>
              <ons-list-item modifier="tappable" ng-click="show(convict)" ng-repeat="convict in filter.convicts">
                {{convict.lastname}}, {{convict.firstname}}
              </ons-list-item>
            </ons-list>

        </ons-page>
    </ons-template>


    <ons-template id="convict.html">
        <ons-page ng-controller="ConvictController">
            <ons-toolbar>
                <div class="left"><ons-back-button>Back</ons-back-button></div>
                <div class="right">
                    <ons-toolbar-button ng-click="menu.toggle()">
                        <ons-icon icon="ion-navicon" size="28px" fixed-width="false"></ons-icon>
                    </ons-toolbar-button>
                </div>
                <div class="center">{{convict.firstname}} {{convict.lastname}}</div>
            </ons-toolbar>

          <div class="formarea">

            <img src="img/portrait.png" style="float: right;"/>
            <ons-list modifier="inset" class="width-half">
              <ons-list-header>Arrival Card</ons-list-header>
              <ons-list-item modifier="tappable" ng-click="show('year', convict.year)">{{convict.arrival}}</ons-list-item>
              <ons-list-item modifier="tappable" ng-click="show('port', convict.port)">From {{convict.port}}</ons-list-item>
              <ons-list-item modifier="tappable" ng-click="show('ship', convict.ship)">On the {{convict.ship}}</ons-list-item>
            </ons-list>

          <ons-list modifier="inset" style="margin-top: 10px">
            <ons-list-item ng-repeat="ref in refs">
              <ons-row>
                <ons-col width="70px">
                  <img src="img/book1.png" width="60" class="ref-image">
                </ons-col>
                <ons-col>
                  <div class="ref-type">{{ref.type}}</div>
                  <div class="ref-code">{{ref.code}}</div>
                </ons-col>
              </ons-row>
            </ons-list-item>

          </div>

        <div style="padding: 10px 9px">
          <ons-button modifier="large">What Happened to {{convict.firstname}}?</ons-button>
        </div>

        </ons-page>
    </ons-template>


    </div>

    <script src="data/data.ships.js"></script>
    <script src="data/data.surnames.js"></script>
    <script src="data/data.ports.js"></script>
    <script src="data/data.years.js"></script>
    <script src="app.js" ></script>

</body>
</html>