/*
* build.js
*
* This file is used within xcode to minimise javacript during build phase.
* 
* @example: node build.js
*/

var compressor = require('yuicompressor');
var fs = require('fs');
var dir = 'www';

if (!fs.existsSync(dir + '/app.js')) {
	console.log('File app.js does not exist');
}
else {
	console.log('Removing debug output..');
	var lines = fs.readFileSync(dir + '/app.js').toString().split("\n");
	var output = [];
	for(i in lines) {
	    if (lines[i].trim().indexOf('console.log') != 0)
	    	output.push(lines[i]);
	}
	console.log('Compressing...');
	compressor.compress(output.join('\n'), {
	    'line-break': 80,
	    nomunge: true
	}, function(err, data, extra) {
	    //err   If compressor encounters an error, it's stderr will be here 
	    //data  The compressed string, you write it out where you want it 
	    //extra The stderr (warnings are printed here in case you want to echo them 
	    if (err) {
	    	console.log(err);
	    } else if (data) {
	    	fs.writeFileSync(dir + "/app.min.js", data);
	    	console.log("Successfully compressed app.min.js");
	    	var index_html = fs.readFileSync(dir + '/index.html').toString().replace('src="app.js"', 'src="app.min.js"');
	    	fs.writeFileSync(dir + '/index.html', index_html);
	    	console.log("Successfully modified index.html");
	    	if (process.env.XCODE_VERSION_MAJOR) {
	    		console.log("Running XCODE.");
		    	fs.unlinkSync(dir + '/app.js');
		    	console.log("Successfully deleted app.js");
		    }
	    }
	});
}
