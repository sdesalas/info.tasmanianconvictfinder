// PARSE.JS
//
// This is used for parsing 85,000 convict records 
// into 16MB of JSON so it can be stored on the device
// for quick lookups.
//

var fs = require('fs'),
    xml2js = require('xml2js');

var parser = new xml2js.Parser();
var start = new Date();
var starttime = start.getTime();
console.log('Start Parsing.. ' + start);
fs.readFile(__dirname + '/Convicts.xml', function(err, data) {
	if (err) {
		console.log(err);
	}
	if (data) {
	    parser.parseString(data, function (err, result) {
	    	var record, id, key, pos, url, output = {};
	    	var records = result.records.record;
			var parse = new Date();
			var parsetime = parse.getTime();
			console.log('End Parsing.. ' + parse);
			console.log('Time To Parse: ' + (parsetime - starttime) / 1000 + ' seconds');
	        console.log('TOTAL: ' + records.length + ' records');
	        for (var i = 0; i < records.length; i++) {
	        	if (i % 10 == 0) { process.stdout.write('.'); }
	        	record = records[i];
	        	id = record.$.id;
	        	key = record.NAME_FULL_DISPLAY[0];
	        	pos = key.indexOf(',');
	        	refs = [];
	        	//console.log(i, record);
	        	if (record.REFERENCE_URL) {
		        	for(var j = 0; j < record.REFERENCE_URL.length; j++) {
		        		url = record.REFERENCE_URL[j];
		        		//console.log(url);
		        		if (url.URL[0].indexOf('http://search.archives.tas.gov.au/ImageViewer/image_viewer.htm?') > -1) {
		        			url = {
		        				t: url.URL_TYPE[0],
		        				f: url.URL[0].replace('http://search.archives.tas.gov.au/ImageViewer/image_viewer.htm?', '')
		        			};
		        			url.c = url.f.replace(/([\w\-]+),(\d+),(\d+),([A-Z]),(\d+)/, '$1');
		        			url.p = url.f.replace(/([\w\-]+),(\d+),(\d+),([A-Z]),(\d+)/, '$3');
		        			refs.push({t: url.t, c: url.c, p: url.p});
		        		}
		        	}
		        }
	        	if (record.YEAR || record.PUBDATE) {
		        	output[id] = {
		        		l: key.substr(0, pos),
		        		f: key.substr(pos + 2),
		        		a: record.ARRIVAL_DATE ? record.ARRIVAL_DATE[0] : (record.YEAR ? record.YEAR[0] : record.PUBDATE[0]),
		        		y: record.YEAR ? record.YEAR[0] : record.PUBDATE[0],
		        		s: record.SHIP ? record.SHIP[0] : '',
		        		p: record.DEPARTURE_PORT ? record.DEPARTURE_PORT[0] : ''
		        	};
		        	if (record.REMARKS) {
		        		output[id].n = record.REMARKS[0];
		        	}
		        	if (refs.length) {
		        		output[id].r = refs;
		        	}
		        }
	        }
	        var end = new Date();
			var endtime = end.getTime();
			console.log('');
	        console.log('End Processing.. ' + end);
			console.log('Time To Process: ' + (endtime - parsetime) / 1000 + ' seconds');
	        //console.log(output);
	        fs.writeFile(__dirname + '/Convicts.js', 'var Convicts = ' + JSON.stringify(output), function() {
	        	console.log('Done');
	        });
	        
	    });
	}

});