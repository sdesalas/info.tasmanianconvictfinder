// PARSE.JS
//
// This is used for parsing 85,000 convict records 
// into SQL Statements for database use
//

var fs = require('fs'),
    xml2js = require('xml2js'),
    util = require('util');

var parser = new xml2js.Parser();
var start = new Date();
var starttime = start.getTime();
console.log('Start Parsing.. ' + start);
fs.readFile(__dirname + '/Convicts.xml', function(err, data) {
	if (err) {
		console.log(err);
	}
	if (data) {
	    parser.parseString(data, function (err, result) {
	    	var record, id, key, pos, url, matches, output = {};
	    	var records = result.records.record;
			var parse = new Date();
			var parsetime = parse.getTime();	
			var sqlformat = function(s) {return s ? (s + '').trim().replace(/'/g, "''") : '';};	
			console.log('End Parsing.. ' + parse);
			console.log('Time To Parse: ' + (parsetime - starttime) / 1000 + ' seconds');
	        console.log('TOTAL: ' + records.length + ' records');
	        try {
		        fs.unlinkSync(__dirname + '/table.refs.sql');
		        fs.unlinkSync(__dirname + '/table.convicts.sql');
		    } catch (e) {}
	        var table_references_insert = 'INSERT INTO refs (id, type, code, page, hasimage) ';
	        var table_convicts_insert = 'INSERT INTO convicts (id, firstname, lastname, arrival, year, ship, port, notes) ';
	        var table_references_sql = [table_references_insert];
	        var table_convicts_sql = [table_convicts_insert];	        
	        for (var i = 0; i < records.length; i++) {
	        	if (i % 10 == 0) { process.stdout.write('.'); }
	        	record = records[i];
	        	id = record.$.id;
	        	key = record.NAME_FULL_DISPLAY[0];
	        	pos = key.indexOf(',');
	        	refs = [];
	        	//console.log(i, record);
	        	if (record.REFERENCE_URL) {
		        	for(var j = 0; j < record.REFERENCE_URL.length; j++) {
		        		url = record.REFERENCE_URL[j];
		        		var added = false;
		        		//console.log(url);
		        		if (url.URL[0].indexOf('http://search.archives.tas.gov.au/ImageViewer/image_viewer.htm?') > -1) {
		        			url.t = url.URL_TYPE[0];
		        			url.f = url.URL[0].replace('http://search.archives.tas.gov.au/ImageViewer/image_viewer.htm?', '');
		        			matches = url.f.match(/([\w\-]+),([\d\-]+),(\d+),([A-Z]),([\d\-]+)/);
		        			if (matches && matches.length > 2) {
			        			url.c = matches[1];
			        			url.p = matches[3];
			        			if (!isNaN(url.p)) {
				        			refs.push({t: url.t, c: url.c, p: url.p});
							        table_references_sql.push(
							        	util.format('SELECT %d, \'%s\', \'%s\', %d, %d', id, url.t, url.c, url.p, 1)
							        );
							        added = true;
					        	}
					        }
		        		}
		        		if (!added) {
		        			url.t = url.URL_TYPE ? url.URL_TYPE[0] : '';
		        			url.f = url.URL_TEXT ? url.URL_TEXT[0] : '';
		        			url.p = null;
		        			matches = url.f.match(/([\w\/]+)\s+page\s+(\d+)/i);
		        			if (matches && matches.length > 1 && !isNaN(matches[2])) {
			        			url.f = matches[1];
				        		url.p = matches[2];
			        		}
					        table_references_sql.push(
					        	util.format('SELECT %d, \'%s\', \'%s\', %d, %d ', id, url.t, url.f, url.p ? url.p : null, 0)
					        );
		        		}
	        			if (table_references_sql.length % 100 == 0 || table_references_sql.length % 100 == 1) {
		        			table_references_sql.push(';');
		        			table_references_sql.push(table_references_insert);
		        		} else {
		        			table_references_sql.push(' UNION ALL ');
		        		}
		        	}
		        }
	        	if (record.YEAR || record.PUBDATE) {
		        	output[id] = {
		        		l: key.substr(0, pos),
		        		f: key.substr(pos + 2),
		        		a: record.ARRIVAL_DATE ? record.ARRIVAL_DATE[0] : (record.YEAR ? record.YEAR[0] : record.PUBDATE[0]),
		        		y: record.YEAR ? record.YEAR[0] : record.PUBDATE[0],
		        		s: record.SHIP ? record.SHIP[0] : '',
		        		p: record.DEPARTURE_PORT ? record.DEPARTURE_PORT[0] : ''
		        	};
		        	if (record.REMARKS) {
		        		output[id].n = record.REMARKS[0];
		        	}
		        	if (refs.length) {
		        		output[id].r = refs;
		        	}
					table_convicts_sql.push(
						util.format(' SELECT %s, \'%s\', \'%s\', \'%s\', %s, \'%s\', \'%s\', \'%s\' ', 
							id, sqlformat(output[id].f), sqlformat(output[id].l), sqlformat(output[id].a), sqlformat(output[id].y), sqlformat(output[id].s), sqlformat(output[id].p), sqlformat(output[id].n))
					);
        			if (table_convicts_sql.length % 100 == 0 || table_convicts_sql.length % 100 == 1 ) {
	        			table_convicts_sql.push(';');
	        			table_convicts_sql.push(table_convicts_insert);
	        		} else {
	        			table_convicts_sql.push(' UNION ALL ');
	        		}
		        }
	        }
	        var end = new Date();
			var endtime = end.getTime();
			console.log('');
	        console.log('End Processing.. ' + end);
			console.log('Time To Process: ' + (endtime - parsetime) / 1000 + ' seconds');
			table_references_sql.pop();
			table_references_sql.push(';');
			table_convicts_sql.pop();
			table_convicts_sql.push(';');
			fs.writeFileSync(__dirname + '/table.refs.sql', table_references_sql.join('\n'));
			fs.writeFileSync(__dirname + '/table.convicts.sql', table_convicts_sql.join('\n'));
			console.log('Done');
	    });
	}

});