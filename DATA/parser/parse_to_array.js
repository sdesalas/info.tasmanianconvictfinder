// PARSE.JS
//
// This is used for parsing 85,000 convict records 
// into 16MB of JSON so it can be stored on the device
// for quick lookups.
//

var fs = require('fs'),
    xml2js = require('xml2js');

var parser = new xml2js.Parser();
var start = new Date();
var starttime = start.getTime();
console.log('Start Parsing.. ' + start);
fs.readFile(__dirname + '/Convicts.xml', function(err, data) {
	if (err) {
		console.log(err);
	}
	if (data) {
	    parser.parseString(data, function (err, result) {
	    	var record, id, key, pos, url, n = 0;
	    	var convict, convicts = [], refs = [];
	    	var ship, ships = {}, surname, surnames = {}, year, years = {}, port, ports={}, arrival;
	    	var records = result.records.record;
			var parse = new Date();
			var parsetime = parse.getTime();
			console.log('End Parsing.. ' + parse);
			console.log('Time To Parse: ' + (parsetime - starttime) / 1000 + ' seconds');
	        console.log('TOTAL: ' + records.length + ' records');
	        for (var i = 0; i < records.length; i++) {
	        	if (i % 10 == 0) { process.stdout.write('.'); }
	        	record = records[i];
	        	convict = {};
	        	id = record.$.id;
	        	key = record.NAME_FULL_DISPLAY[0];
	        	pos = key.indexOf(',');
	        	//console.log(i, record);
	        	if (record.REFERENCE_URL) {
		        	for(var j = 0; j < record.REFERENCE_URL.length; j++) {
		        		url = record.REFERENCE_URL[j];
		        		//console.log(url);
		        		if (url.URL[0].indexOf('http://search.archives.tas.gov.au/ImageViewer/image_viewer.htm?') > -1) {
		        			url = {
		        				t: url.URL_TYPE[0],
		        				f: url.URL[0].replace('http://search.archives.tas.gov.au/ImageViewer/image_viewer.htm?', '')
		        			};
		        			url.c = url.f.replace(/([\w\-]+),(\d+),(\d+),([A-Z]),(\d+)/, '$1');
		        			url.p = url.f.replace(/([\w\-]+),(\d+),(\d+),([A-Z]),(\d+)/, '$3');
		        			refs.push({n: n, t: url.t, c: url.c, p: Number(url.p)});
		        		} else {
		        			refs.push({n: n, url.URL_TYPE ? url.URL_TYPE[0] : undefined, c: url.URL_TEXT ? url.URL_TEXT[0] : undefined});
		        		}
		        	}
		        }
	        	if (record.YEAR || record.PUBDATE) {
			        ship = record.SHIP ? record.SHIP[0] : 'Unknown';
			        port = record.DEPARTURE_PORT ? record.DEPARTURE_PORT[0] : 'Unknown';
			        surname = key.substr(0, pos);
			        year = Number(record.YEAR ? record.YEAR[0] : record.PUBDATE[0]);
			        arrival = record.ARRIVAL_DATE ? record.ARRIVAL_DATE[0] : (record.YEAR ? record.YEAR[0] : record.PUBDATE[0]);
	        		convict = {
		        		l: surname,
		        		f: key.substr(pos + 2),
		        		a: arrival,
		        		y: year,
		        		s: ship,
		        		p: port
	        		};
		        	if (record.REMARKS) {
		        		convict.n = record.REMARKS[0];
		        	}
		        	if (ship) {
		        		if (!ships[ship]) {ships[ship] = 0;}
		        		ships[ship]++;
		        	}
		        	if (port) {
		        		if (!ports[port]) {ports[port] = 0;}
		        		ports[port]++;
		        	}
		        	if (surname) {
		        		if (!surnames[surname]) {surnames[surname] = 0;}
		        		surnames[surname]++;
		        	}
		        	if (year) {
		        		if (!years[year]) {years[year] = {};}
		        		if (!years[year][arrival]) {years[year][arrival] = 0}
		        		years[year][arrival]++;
		        	}
		        	//if (refs.length) {
		        	//	convict.r = refs;
		        	//}
			        convicts.push(convict);
			        n++;
		        }
	        }
	        var end = new Date();
			var endtime = end.getTime();
			var sortKeys = function (obj, func) {
				keys = !!func ? Object.keys(obj).sort(func) : Object.keys(obj).sort();
			    return keys.reduce(function (result, key) {
			        result[key] = obj[key];
			        return result;
			    }, {});
			}
			console.log('');
			console.log('Total: ' + convicts.length + ' records.');
			console.log('');
	        console.log('End Processing.. ' + end);
			console.log('Time To Process: ' + (endtime - parsetime) / 1000 + ' seconds');
	        //console.log(output);
	        ships = sortKeys(ships);
	        ports = sortKeys(ports);
	        surnames = sortKeys(surnames);
	        years = sortKeys(years);
	        for (var year in years) {
	        	years[year] = sortKeys(years[year], function(x, y) {
	        		return new Date(x) - new Date(y);
	        	});
	        }
	        fs.writeFileSync(__dirname + '/data.convicts.js', 'var data = data || {}; data.convicts = ' + JSON.stringify(convicts));
	        fs.writeFileSync(__dirname + '/data.refs.js', 'var data = data || {}; data.refs = ' + JSON.stringify(refs));
	        fs.writeFileSync(__dirname + '/data.ships.js', 'var data = data || {}; data.ships = ' + JSON.stringify(ships));
	        fs.writeFileSync(__dirname + '/data.ports.js', 'var data = data || {}; data.ports = ' + JSON.stringify(ports));
	        fs.writeFileSync(__dirname + '/data.surnames.js', 'var data = data || {}; data.surnames = ' + JSON.stringify(surnames));
	        fs.writeFileSync(__dirname + '/data.years.js', 'var data = data || {}; data.years = ' + JSON.stringify(years));
	        console.log('Done');
	    });
	}

});